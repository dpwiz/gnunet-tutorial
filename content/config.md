---
title: Configuring
---

> Don't have a working node at hand?
> Check out [installation instructions](/install/).
>
> Up and running already?
> Proceed to have fun with [services](/services/)!

Congratulations! GNUnet is now installed! Before we start it we need to create a configuration file. By default GNUnet looks in our home directory for the file `~/.gnunet/gnunet.conf`. We can start with an empty file for now:

```sh
touch ~/.config/gnunet.conf
```

## Configuration tools

TBD.

### Editing the config file

TBD.

### Command-line

TBD.

### Graphical

TBD.

## Startup

TBD.

### Manual

TBD.

### Systemd service

TBD.

### SystemV service

TBD.

## Limits

It's reccomended that you increase your bandwidth restrictions from the acutely low defaults. The example below sets the WAN and LAN limits to the value `unlimited`.

```sh
gnunet-config -s ats -o WAN_QUOTA_IN -V unlimited
gnunet-config -s ats -o WAN_QUOTA_OUT -V unlimited
gnunet-config -s ats -o LAN_QUOTA_IN -V unlimited
gnunet-config -s ats -o LAN_QUOTA_OUT -V unlimited
```

## Services

### Common

TBD.

## Identities