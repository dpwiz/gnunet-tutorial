---
title: Installation
---

> Attention: If you came across the official package in your OS, ignore it!
> It is most likely ancient and not compatible with current GNUnet installations in the network.

## Requirements

There are some common parts and some OS-specific instructions pick one
that suits your environment and proceed with it.

### Ubuntu 18.04

```sh
sudo apt install git libtool autoconf autopoint \
build-essential libgcrypt-dev libidn11-dev zlib1g-dev \
libunistring-dev libglpk-dev miniupnpc libextractor-dev \
libjansson-dev libcurl4-gnutls-dev libsqlite3-dev openssl \
libnss3-tools libgnutls28-dev
```

### Debian 9

```shell
sudo apt install git libtool autoconf autopoint \
build-essential libgcrypt-dev libidn11-dev zlib1g-dev \
libunistring-dev libglpk-dev miniupnpc libextractor-dev \
libjansson-dev libcurl4-gnutls-dev libsqlite3-dev openssl \
libnss3-tools libgnutls28-dev
```

### Archlinux/Pi

First you need the hardware. This tutorial works with a Raspberry Pi 3. The other Pi versions I haven't tested, but they should probably work, too. To install Archlinux on the Pi3, follow the instructions from [archlinuxarm.org](https://archlinuxarm.org/platforms/armv8/broadcom/raspberry-pi-3).

That was easy, right? Our goal now is to build GNUnet from source. So, let's install the tools for building new software.

```shell
sudo pacman -S base-devel
```

Now we can continue to install the following Archlinux dependencies to compile GNUnet on our Pi.

```shell
pacman -Su libmicrohttpd libgcrypt gnurl libunistring gnutls libidn libextractor openssl libltdl sqlite texinfo which gettext zlib pkg-config git miniupnpc libextractor jansson nim
```

### MacOS 10.14 (Mojave)

> As a bare minimum, we recommend you install homebrew and XCode before reading any further.

```shell
brew install git autoconf automake glpk gettext gnutls jansson libextractor libgcrypt libffi libidn2 libmicrohttpd libmpc libtool libunistring pkg-config unbound
```

### NetBSD 8.0 CURRENT

As a bare minimum, we recommend you install `pkgsrc`. This tutorial assumes you have the CURRENT version checked out. Additionally you might want to install `devel/git-base` and `git clone pkgsrc-wip` into the `wip` folder in your `pkgsrc` folder:

```shell
cd /usr/pkgsrc
git clone git://wip.pkgsrc.org/pkgsrc-wip.git wip
```

GNUnet is not yet in the pkgsrc tree, it's still in the pkgsrc-wip repository. So if you checked out pkgsrc-wip into `/usr/pksrc/wip`:

```shell
cd /usr/pkgsrc/wip/gnunet
```

Before you start building, you might want to review your `/etc/mk.conf`.
I suggest to consider:

```
WRKOBJDIR=/usr/work
DISTDIR=/usr/distfiles
DEPENDS_TARGET=package
UPDATE_TARGET=bin-install
ACCEPTABLE_LICENSES+= gnu-agpl-v3
MKREPRO=yes
```

As we rely on ImageMagick somewhere down our dependency chain (it is one of libextractor's dependencies), you need to set the following (as of 2019-03-06) as well:

```
IGNORE_URL+=https://nvd.nist.gov/vuln/detail/CVE-2018-15607
```

You could however just `env ALLOW_VULNERABLE_PACKAGES=1`.

The build process can involve manual installation interruptions for dependencies!

Then start building:

```shell
make package
```

Once you are done, type as root:

```shell
make install
```

or alternatively:

```shell
pkg_add /usr/pkgsrc/packages/All/gnunet-0.11.0.tgz
```

And you're done! Feel free to skip the rest of the chapter.

> Note that the rc.d file which is installed is not yet functional.

## Make an installation directory

Next we create a directory in our home directory where we store the source code later. We should keep this directory after installation because it contains Makefiles that can be used for uninstalling GNUnet again (see chapter *Uninstall GNUnet and its dependencies*).

```sh
mkdir ~/gnunet_installation
```

## Get the source code

```shell
cd ~/gnunet_installation
git clone --depth 1 https://gnunet.org/git/gnunet.git
git clone --depth 1 https://gnunet.org/git/libmicrohttpd.git
```

> You may wish to review available tags and check out code at a most recent tag instead of `master` itself by adding `--branch v0.XX.Y`.

## Compile and Install

The sources would be compiled in the checkout directory, and the installation would proceed under the path specified:

```shell
export GNUNET_PREFIX=/opt
```

### libmicrohttpd

Before we can compile GNUnet, we compile and install a fresh version of `libmicrohttpd`.

> Alternatively, you can skip this step and use your OS development package for it.
> When you do this, remove the `--with-microhttp` in the next step.

```shell
cd ~/gnunet_installation/libmicrohttpd
autoreconf -fi
./configure --disable-doc --prefix="$GNUNET_PREFIX/libmicrohttpd"
make -j$(nproc || echo -n 1)
sudo make install
```

### GNUnet

First, run bootstrapping script to prepare autotools configuration:

```
cd ~/gnunet_installation/gnunet
./bootstrap
```

Now, set your compilation options and let the autotools do its thing.

> To have a more debug-friendly build of GNUnet set CFLAGS to `"-g -Wall -O0"` and add `--enable-logging=verbose` to the `configure` command.

```shell
export CFLAGS="-O2"

./configure --disable-documentation --with-microhttpd="$GNUNET_PREFIX/libmicrohttpd" --prefix="$GNUNET_PREFIX/gnunet"
```

It should finish without errors if the **Requirements** section is up to date.
Please consult your fellow OS users on how to install packages required
and report a bug against this tutorial.

Everything is ready now to compile the GNUnet code:

```shell
make -j$(nproc || echo -n 1)
```

Finally, invoke your superuser privileges to install the compiled code
under a prefix specified.

```shell
sudo make install
```

> You may wish to add the `/bin` subdirectory of installation path to `PATH` environment variable, for example in `~/.bashrc`.

> You can now proceed with generic [configuration instructions](../../config/).

<!--
sudo addgroup gnunetdns
sudo adduser --system --group --disabled-login --home /var/lib/gnunet gnunet
-->

## Uninstall GNUnet and its dependencies

If you wish to remove the currently installed binaries (e.g. for clean-slate upgrates) go to a build directory and invoke make command:

```shell
cd ~/gnunet_installation/gnunet
sudo make uninstall
cd ~/gnunet_installation/libmicrohttpd
sudo make uninstall
```
