## Introduction

Welcome to the hopefully painless GNUnet tutorial for Ubuntu 18.04! It provides very concrete instructions on how to compile, install and configure a current version of GNUnet. The goal is to support newcomers, either end users or developers, who want to get in touch with GNUnet for the first time. After installing GNUnet we will make sure that out new GNUnet installation is working correctly.

> **Attention**: If you came across the official gnunet package for Ubuntu 18.04, ignore it! It is ancient and not compatible with current GNUnet installations.
